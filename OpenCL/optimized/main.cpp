#include <algorithm>
#include <cp_image.h>
#include <cp_lib.h>
#include <cp_params.h>
#include <opencl_api.h>
#include <opencl_utility.h>

cp::image
expand_image(const cp::image& image)
{
    cp::image ret_val;
    ret_val.width = image.width + 10;
    ret_val.height = image.height + 10;
    ret_val.pixels.resize(ret_val.height * ret_val.width * 4, 0);

    for (std::size_t i = 0; i < image.width * 4; i += 4)
    {
        for (std::size_t j = 0; j < image.height * 4; j += 4)
        {
            std::size_t index = (j + 5 * 4) * (image.width + 10) + i + 5 * 4;
            std::size_t orig_index = (j * image.width) + i;

            if (i == 0 && j == 0)
            CP_DEBUG("i: %zu, j: %zu, index: %zu, orig_index: %zu",
                     i, j, index, orig_index);

            ret_val.pixels[index + 0] = image.pixels[orig_index + 0];
            ret_val.pixels[index + 1] = image.pixels[orig_index + 1];
            ret_val.pixels[index + 2] = image.pixels[orig_index + 2];
            ret_val.pixels[index + 3] = image.pixels[orig_index + 3];

        }
    }
    return ret_val;
}

cp::image
retract_image(const cp::image& image)
{
    return image;
//    cp::image ret_val;
//    ret_val.width = image.width - 10;
//    ret_val.height = image.height - 10;
//    ret_val.pixels.resize(ret_val.height * ret_val.width * 4, 0);
//
//    for (std::size_t i = 0; i < ret_val.width * 4; i += 4)
//    {
//        for (std::size_t j = 0; j < ret_val.height * 4; j += 4)
//        {
//            std::size_t orig_index = (j + 5 * 4) * (ret_val.width) + i + 5 * 4;
//            std::size_t index = (j * ret_val.width) + i;
//
//            if (i == 0 && j == 0)
//            CP_DEBUG("i: %zu, j: %zu, index: %zu, orig_index: %zu",
//                     i, j, index, orig_index);
//
//            ret_val.pixels[index + 0] = image.pixels[orig_index + 0];
//            ret_val.pixels[index + 1] = image.pixels[orig_index + 1];
//            ret_val.pixels[index + 2] = image.pixels[orig_index + 2];
//            ret_val.pixels[index + 3] = image.pixels[orig_index + 3];
//
//        }
//    }
//
//    CP_DEBUG("Returning");
//    return ret_val;
}

int
main(CP_UNUSED int argc,
     CP_UNUSED char** argv)
{
    namespace cpcl = cp::opencl;
    cp_log_init();

    auto params = cp::parse_cmd(argc, argv,
                                "assets/exam_photo.png",
                                "assets/output.png",
                                false);

    cpcl::api api(params.device_type,
                  "kernels/optimized.cl",
                  "-Ikernels/cp_lib/ -Werror -cl-std=CL1.2");

    auto h_input_image = expand_image(cp::load_image(params.in_filepath.c_str(), LCT_RGBA));
    //cp::write_image(params.out_filepath.c_str(), h_input_image,  LCT_RGBA);

    cl_image_format image_format;
    image_format.image_channel_order = CL_RGBA;
    image_format.image_channel_data_type = CL_UNSIGNED_INT8;

    cl_image_desc image_description;
    image_description.image_type = CL_MEM_OBJECT_IMAGE2D;
    image_description.image_width = h_input_image.width;
    image_description.image_height = h_input_image.height;
    image_description.image_row_pitch = h_input_image.width * 4;
    image_description.image_slice_pitch = h_input_image.height * 4;
    image_description.num_mip_levels = 0;
    image_description.num_samples = 0;
    image_description.buffer = nullptr;

    cpcl::buffer d_input_image =
        cpcl::create_resource(clCreateBuffer,
                              api.context(),
                              CL_MEM_READ_ONLY |
                              CL_MEM_COPY_HOST_PTR |
                              CL_MEM_HOST_NO_ACCESS,
                              sizeof(std::uint8_t) * h_input_image.pixels.size(),
                              h_input_image.pixels.data());

    image_description.image_row_pitch = 0;
    image_description.image_slice_pitch = 0;

    cp::opencl::buffer d_greyscale_output =
        cp::opencl::create_resource(clCreateBuffer,
                                    api.context(),
                                    CL_MEM_READ_WRITE |
                                    CL_MEM_HOST_NO_ACCESS,
                                    sizeof(std::uint8_t) * (h_input_image.width) * (h_input_image.height),
                                    nullptr);

    cpcl::buffer d_output_image =
        cpcl::create_resource(clCreateBuffer,
                              api.context(),
                              CL_MEM_READ_WRITE |
                              CL_MEM_COPY_HOST_PTR |
                              CL_MEM_HOST_READ_ONLY,
                              sizeof(std::uint8_t) * h_input_image.pixels.size(),
                              h_input_image.pixels.data());

    cpcl::kernel greyscale_kernel =
        cpcl::create_resource(clCreateKernel,
                              api.program(),
                              "greyscale");

    cpcl::invoke(clSetKernelArg,
                 greyscale_kernel.data(),
                 0,
                 sizeof(cl_mem),
                 &d_input_image.data());

    cpcl::invoke(clSetKernelArg,
                 greyscale_kernel.data(),
                 1,
                 sizeof(cl_mem),
                 &d_greyscale_output.data());

    cpcl::invoke(clSetKernelArg,
                 greyscale_kernel.data(),
                 2,
                 sizeof(std::int32_t),
                 &h_input_image.width);

    cpcl::kernel detection_kernel =
        cpcl::create_resource(clCreateKernel,
                              api.program(),
                              "detect_white_rects");

    cpcl::invoke(clSetKernelArg,
                 detection_kernel.data(),
                 0,
                 sizeof(cl_mem),
                 &d_greyscale_output.data());

    cpcl::invoke(clSetKernelArg,
                 detection_kernel.data(),
                 1,
                 sizeof(cl_mem),
                 &d_output_image.data());

    cpcl::invoke(clSetKernelArg,
                 detection_kernel.data(),
                 2,
                 sizeof(std::int32_t),
                 &h_input_image.width);

    CP_DEBUG("Before greyscale");
    std::size_t work_size[] = {h_input_image.width - 10, h_input_image.height - 10};
    cpcl::invoke(clEnqueueNDRangeKernel,
                 api.queue(), greyscale_kernel.data(),
                 2, nullptr,
                 work_size,
                 nullptr, 0, nullptr, nullptr);
    cpcl::invoke(clFinish, api.queue());

    CP_DEBUG("Before detection");
    cpcl::invoke(clEnqueueNDRangeKernel,
                 api.queue(), detection_kernel.data(),
                 2, nullptr,
                 work_size,
                 nullptr, 0, nullptr, nullptr);
    cpcl::invoke(clFinish, api.queue());


    CP_DEBUG("Finished running kernel");

    cp::image h_output_image;
    h_output_image.width = h_input_image.width;
    h_output_image.height = h_input_image.height;
    h_output_image.pixels.resize(h_output_image.width * h_output_image.height * 4);

    std::size_t origin[3] = {5,5,0};
    std::size_t region[3] = {h_input_image.width - 5, h_input_image.height - 5, 1};


    cpcl::invoke(clEnqueueReadBuffer,
                 api.queue(),
                 d_output_image.data(),
                 CL_TRUE,
                 0,
                 sizeof(std::uint8_t) * h_output_image.pixels.size(),
                 h_output_image.pixels.data(),
                 0, nullptr, nullptr);

    cp::write_image(params.out_filepath.c_str(),
                    retract_image(h_output_image),
                    LCT_RGBA);

    CP_DEBUG("Shutdown");

    cp_log_shutdown();

    return 0;
}
