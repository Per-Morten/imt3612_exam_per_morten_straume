cmake_minimum_required (VERSION 2.6)
project (run_kernel)

set (opencl_implementation_VERSION_MAJOR 1)
set (opencl_implementation_VERSION_MINOR 0)

include_directories(run_kernel
    ${CMAKE_CURRENT_SOURCE_DIR}
)

add_executable(run_kernel
    main.cpp
)


target_link_libraries(run_kernel
    ${CP_STANDARD_LINKS}
    cp_lib
    opencl_lib
)
