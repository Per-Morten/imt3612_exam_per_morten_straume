#include <cp_lib_types.h>

__constant sampler_t rects_sampler = CLK_NORMALIZED_COORDS_FALSE |
                                     CLK_ADDRESS_CLAMP |
                                     CLK_FILTER_NEAREST;

__constant const float compare_weights[11][11] =
{
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
    {-1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f, -1.5f},
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
};

#define MATRIX_SIZE convert_int((sizeof(compare_weights[0]) / sizeof(float)))

__kernel void detect_white_rects(__read_only image2d_t in_image,
                                 __write_only image2d_t out_image,
                                 __read_only image2d_t original_image)
{
    int2 pos = {get_global_id(0), get_global_id(1)};

    float sum[4] = {0.0f};

    for (float angle = 0.0f; angle < M_PI_F; angle += M_PI_4_F)
    {
        for (int32_t i = -MATRIX_SIZE / 2; i <= MATRIX_SIZE / 2; ++i)
        {
            for (int32_t j = -MATRIX_SIZE / 2; j <= MATRIX_SIZE / 2; ++j)
            {
                float4 values = convert_float4(read_imageui(in_image,
                                               rects_sampler,
                                               (int2)(pos.x + i * cos(angle),
                                                      pos.y + j * sin(angle))));

                sum[convert_uint(angle / M_PI_4_F)] +=
                    values.x * compare_weights[j + MATRIX_SIZE / 2][i + MATRIX_SIZE / 2];
            }
        }
    }

    uint4 colors = read_imageui(original_image, rects_sampler, pos);

    for (size_t i = 0; i < 4; ++i)
    {
        if (sum[i] / 35 > 180)
        {
            colors.x = convert_uchar(sum[i] / 35);
            colors.y = 0;
            colors.z = 0;
            colors.w = 255;
        }
    }

    write_imageui(out_image, pos, colors);
}

__constant const uint8_t threshold = 180;

__kernel void greyscale(__read_only image2d_t in_image,
                        __write_only image2d_t out_image)
{
    int2 pos = {get_global_id(0), get_global_id(1)};

    uint4 colors = read_imageui(in_image, pos);

    uint8_t avg = (colors.x < threshold &&
                   colors.y < threshold &&
                   colors.z < threshold)
                ? 0
                : 255;

    colors.x = avg;
    colors.y = avg;
    colors.z = avg;

    write_imageui(out_image, pos, colors);
}

