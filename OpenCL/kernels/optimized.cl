#include <cp_lib_types.h>

__constant sampler_t rects_sampler = CLK_NORMALIZED_COORDS_FALSE |
                                     CLK_ADDRESS_CLAMP |
                                     CLK_FILTER_NEAREST;

__constant const float compare_weights[11][11] =
{
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
    {-1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f,   1.0f, -1.5f},
    {-1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f,  -1.5f, -1.5f},
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
    { 0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,   0.0f,  0.0f},
};

#define MATRIX_SIZE convert_int((sizeof(compare_weights[0]) / sizeof(float)))

__kernel void detect_white_rects(__global uint8_t* in_field,
                                 __global uint8_t* out_image,
                                 int32_t width)
{
    int2 pos = {get_global_id(0), get_global_id(1)};

    float sum[4] = {0.0f};

    //const int32_t width = get_image_width(out_image);

    for (float angle = 0.0f; angle < M_PI_F; angle += M_PI_4_F)
    {
        for (int32_t i = -MATRIX_SIZE / 2; i <= MATRIX_SIZE / 2; ++i)
        {
            for (int32_t j = -MATRIX_SIZE / 2; j <= MATRIX_SIZE / 2; ++j)
            {
                int2 indices = (int2)(pos.x + 5 + i * cos(angle), pos.y + 5 + j * sin(angle));
                int32_t index = indices.y * (width) + indices.x;


                sum[convert_uint(angle / M_PI_4_F)] +=
                    in_field[index] * compare_weights[j + MATRIX_SIZE / 2][i + MATRIX_SIZE / 2];
            }
        }
    }


    int2 indices = {pos.x + 5, pos.y + 5};
    int32_t index = indices.y * (width) + indices.x;
    if (pos.x == 0 && pos.y == 0)
        printf("Index: %d\n", index);
    uint4 colors;//{in_field[index], in_field[index], in_field[index], 255};
    colors.x = in_field[index];
    colors.y = in_field[index];
    colors.z = in_field[index];
    colors.w = 255;


    for (size_t i = 0; i < 4; ++i)
    {
        if (sum[i] / 35 > 180)
        {
            //printf("sum: %f\n", sum[i]);
            colors.x = convert_uchar(sum[i] / 35);
            colors.y = 0;
            colors.z = 0;
            colors.w = 255;
        }
    }

    //int32_t index2 = (pos.y * 4 + 5 * 4) * (width) + pos.x * 4 + 5 * 4;
    int32_t index2 = index * 4;
    if (pos.x == 0 && pos.y == 0)
        printf("Index2: %d\n", index2);
    out_image[index2 + 0] = colors.x;
    out_image[index2 + 1] = colors.y;
    out_image[index2 + 2] = colors.z;
    out_image[index2 + 3] = colors.w;
}



__constant const uint8_t threshold = 180;

__kernel void greyscale(__global uint8_t* restrict in_image,
                        __global uint8_t* restrict out_field,
                        int32_t width)
{
    int2 pos = {get_global_id(0), get_global_id(1)};

    size_t color_index = (pos.y * 4 + 5 * 4) * (width) + pos.x * 4 + 5 * 4;

    uint4 colors =
    {
        in_image[color_index + 0],
        in_image[color_index + 1],
        in_image[color_index + 2],
        in_image[color_index + 3],
    };

    size_t index = (pos.y + 5) * (width) + pos.x + 5;
    if (pos.x == 0 && pos.y == 0)
    {
        printf("color_index: %zu\n", index);
    }

    out_field[index] = (colors.x < threshold &&
                        colors.y < threshold &&
                        colors.z < threshold)
                     ? 0
                     : 255;
}


