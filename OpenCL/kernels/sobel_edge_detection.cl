#include <cp_lib_types.h>

__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
                               CLK_ADDRESS_CLAMP_TO_EDGE |
                               CLK_FILTER_NEAREST;

__constant const float x_matrix[3][3] =
{
    { 1,  0, -1},
    { 2,  0, -2},
    { 1,  0, -1},
};

__constant const float y_matrix[3][3] =
{
    { 1,  2,  1},
    { 0,  0,  0},
    {-1, -2, -1},
};

__kernel void sobel_edge_detection(__read_only image2d_t in_image,
                                   __write_only image2d_t out_image)
{
    int2 pos = {get_global_id(0), get_global_id(1)};

    float x_delta;
    float y_delta;
    for (int32_t i = -1; i <= 1; ++i)
    {
        for (int32_t j = -1; j <= 1; ++j)
        {
            float color = read_imageui(in_image,
                                       sampler,
                                       (int2)(pos.x + i, pos.y + j)).x;

            x_delta = color * x_matrix[i + 1][j + 1];
            y_delta = color * y_matrix[i + 1][j + 1];
        }
    }

    x_delta /= 4.0f;
    y_delta /= 4.0f;

    float sum_delta = sqrt(pow(x_delta,2.0f) + pow(y_delta,2.0f));
    uint32_t strength = (sum_delta);

    if (pos.x == 256 && pos.y == 256)
    {
        printf("x_delta: %f, y_delta: %f, strength: %u\n", x_delta, y_delta, strength);
    }

    uint4 output_color = {strength, strength, strength, 255};

    write_imageui(out_image, pos, output_color);
}
