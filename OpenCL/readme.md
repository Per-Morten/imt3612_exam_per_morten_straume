## OpenCL/Cuda

The task for the OpenCL/Cuda group is an image processing task related to detecting objects in stalite images.
There are been a lot of interest recently in using the newly available 30cm resolution statelite image system to detect everything from trees, to birds.
The higher resolution also makes brute force GPU algorithms more useful as both the amount of data has increased and the detail in the images.

### The task

1. Download the full resoluion image of Bondi beach. [http://www.satimagingcorp.com/gallery/geoeye-1/geoeye-1-bondi-beach-australia/](http://www.satimagingcorp.com/gallery/geoeye-1/geoeye-1-bondi-beach-australia/)
1. You can use either Hough like cube recognition or Haar like features to detect **White cars** [https://software.intel.com/en-us/node/504530?language=ru]
1. The detection of cars should put a red dot in the center of most of the white cars on the image. You do not have to worry about vans at this stage, and some misclassification is fine.
1. You do not have to get 100% accuracy. You must discuss the parallelism in the task and how you devided the large image for processing.
1. If you make improvements discuss brute force processing vs algorithmic complexity.

###Improvements

1. Indentify other cars as well
1. Indentify the orientation of the car
1. Try counting the number of people on the beach.
1. Discuss various scales and detection of houses


There are other images for testing at:
[https://aws.amazon.com/public-datasets/landsat/]