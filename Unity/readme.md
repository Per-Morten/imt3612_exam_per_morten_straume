# Atom simulation

For the Unity 3D exam we would like to see a GPU renderer that visualises atoms in molecules.  
The traditional teaching material for atoms and molecules have spheres with connectors.

You should implement both visualling interesting representations, as well as working out how to make the shader represent the atoms in a more realistic way.

Here is the image of a hydrogen orbit of the electron. 

![image](https://i.kinja-img.com/gawker-media/image/upload/s--a-AF_4lF--/c_scale,fl_progressive,q_80,w_800/18ontxblfw77lpng.png)
<img src="https://i.kinja-img.com/gawker-media/image/upload/s--a-AF_4lF--/c_scale,fl_progressive,q_80,w_800/18ontxblfw77lpng.png" width="200" \>

You should google for other representation of atoms to see if you can make a good looking represenation. 
You will be graded on the way you use multiple shaders to achieve a particular look. 
You must reference the location you got the idea for you represenation from


In this exam you will:

 * Make a line of spheres each a hydrongen atom.
 * Add GPU shaders to each of the sheres with atom visualisations - one must try to be a pulsating version of the above image
 * Make a line of Oxygen Atoms with a different representation - oxygen has two layers and internal and an external - you can add a separate sphere for that.
 * Discuss the complexity of the shader
 * add hundreds or thousands of these spheres to stress the graphics system. 
   * you report the number of spheres and the frame rate
   * evaluate the different shaders for their time cost by running many copies.

