## Vulkan
One of the interesting developments for rendering in Virtual Reality is the concept of **foveated rendering**.  
This is where you render specific areas of the screen at full resolution, and the rest at a lower resolution.

For this exam task we want you to create a Vulkan implementation of a foveated renderer where:

  * The center is rendered at full resolution
  * The surround is blurred to save rendering time
  * the size of the center can **initially** be the center 1/3 of the image, Thus a 800x600 image will have 266x200 squares with the square from 266,200 to 532,400 being full resolution
  * the suround tiles will use various solutions for blurring, you can experiment to find the fastest/best
  	1. One of the options is simple lookup, only calculate every second pixel and copy the value to the next pixel
	1. Use a linear interpolation between pixels to create smoother averages. 

## Additional 
Interesting things to try if you have time

  * The fovea is located at the mouse position on screen
  * The surround is either averaged of nearest neighbour calculation for generating the image surround the fovea
  * You edit the tesselation shader so that it only tesselates near the fovea

This is where you describe your approach to the problem, the challenges you encountered and how you solved the.
